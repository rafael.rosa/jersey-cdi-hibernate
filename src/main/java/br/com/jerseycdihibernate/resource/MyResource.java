package br.com.jerseycdihibernate.resource;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.jerseycdihibernate.model.City;
import br.com.jerseycdihibernate.service.ICityService;
import br.inf.jerseycdihibernate.util.transactionHibernate.Transactional;

@Path("cities")
@RequestScoped
public class MyResource {
    
    @Inject 
    private ICityService cityService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public List<City> message() {
        
        List<City> cities = cityService.findAll();

        return cities;
    }
}