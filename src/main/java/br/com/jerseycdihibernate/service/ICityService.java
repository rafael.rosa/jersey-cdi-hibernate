package br.com.jerseycdihibernate.service;

import java.util.List;

import br.com.jerseycdihibernate.model.City;

public interface ICityService {
    
    public List<City> findAll();
}