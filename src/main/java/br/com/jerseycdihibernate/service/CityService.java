package br.com.jerseycdihibernate.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;

import br.com.jerseycdihibernate.model.City;

public class CityService implements ICityService, Serializable {
	private static final long serialVersionUID = 4771783341800028249L;

	@Inject
	private Session session;
	
    @Override
    public List<City> findAll() {
    	Criteria criteria = session.createCriteria(City.class);
		//criteria.addOrder(Order.asc(Pagina.Fields.CODIGO.toString()));
    	List<City> response = criteria.list();
		if(response.isEmpty()) {
			List<City> cities = new ArrayList<>();

	        cities.add(new City("Bratislava", 432000));
	        cities.add(new City("Budapest", 1759000));
	        cities.add(new City("Prague", 1280000));
	        cities.add(new City("Warsaw", 1748000));
	        cities.add(new City("Los Angeles", 3971000));
	        cities.add(new City("New York", 8550000));
	        cities.add(new City("Edinburgh", 464000));
	        cities.add(new City("Berlin", 3671000));

	        cities.forEach(city -> { 
	        	session.saveOrUpdate(city);
	        });
	        
	        response = criteria.list();
		}

		return response;
    }
}
